# Elektronika ir Elektrotechnika 2023

*The IEEE 27th International Conference ELECTRONICS 2023 (#electronicsconf) will take place in Palanga, Lithuania from 19th to 21th June, 2023 and online via video conferencing system. ELECTRONICS 2023 will combine an in-person event with online components, and participants will be able to choose to participate either in person or virtually.*

Link to the conference: http://electronicsconf.ktu.edu/index.php/elc/index </br>

Link to the journal ELEKTRONIKA IR ELEKTROTECHNIKA (ISSN 1392-1215): https://eejournal.ktu.lt/index.php/elt

Author's Guarantee Form: https://gitlab.com/momcilo_krunic/elektronika_ir_elektrotechnika_2023/-/blob/main/resources/warrant/signed-Authors_Guarantee_Form.pdf

PDF: https://momcilo_krunic.gitlab.io/elektronika_ir_elektrotechnika_2023/Documentation_as_Code_in_Automotive_System_Software_Engineering.pdf

DOCX: https://momcilo_krunic.gitlab.io/elektronika_ir_elektrotechnika_2023/Documentation_as_Code_in_Automotive_System_Software_Engineering.docx

ODT: https://momcilo_krunic.gitlab.io/elektronika_ir_elektrotechnika_2023/Documentation_as_Code_in_Automotive_System_Software_Engineering.odt

