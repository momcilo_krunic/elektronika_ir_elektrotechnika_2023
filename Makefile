BUILDDIR=public
FILENAME=Documentation_as_Code_in_Automotive_System_Software_Engineering

all: pdf docx odt

pdf:
	mkdir $(BUILDDIR) -p  # Creates the BUILDDIR if it doesn't already exist.
	pandoc $(FILENAME).md \
	--filter pandoc-crossref \
	--citeproc \
	--from=markdown+tex_math_single_backslash+tex_math_dollars+raw_tex \
	--to=latex \
	--output=$(BUILDDIR)/$(FILENAME).pdf \
	--pdf-engine=pdflatex \
	--variable urlcolor=blue \
	--fail-if-warnings

docx:
	mkdir $(BUILDDIR) -p  # Creates the BUILDDIR if it doesn't already exist.
	pandoc $(FILENAME).md \
	--filter pandoc-crossref \
	--citeproc \
	--from=markdown+tex_math_single_backslash+tex_math_dollars+raw_tex \
	--to=docx \
	--output=$(BUILDDIR)/$(FILENAME).docx \
	--pdf-engine=pdflatex \
	--reference-doc=resources/template.docx \
	--variable urlcolor=blue \
	--fail-if-warnings

odt:
	mkdir $(BUILDDIR) -p  # Creates the BUILDDIR if it doesn't already exist.
	pandoc $(FILENAME).md \
	--filter pandoc-crossref \
	--citeproc \
	--from=markdown+tex_math_single_backslash+tex_math_dollars+raw_tex \
	--to=odt \
	--output=$(BUILDDIR)/$(FILENAME).odt \
	--reference-doc=resources/template.odt \
	--variable urlcolor=blue \
	--fail-if-warnings
